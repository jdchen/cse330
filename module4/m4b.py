# declare "Player" class
class Player:
	def __init__(self,name):
		#variables that describe a player
		self.name=name
		self.atbats=0
		self.hits=0
		self.runs=0

	def addAtbats(self,n):
		self.atbats=self.atbats+n

	def addHits(self,n):
		self.hits=self.hits+n

	def addRuns(self,n):
		self.runs=self.runs+n

	#create getter methods to obtain information about a player
	def getName(self):
		return self.name

	def getBattingAverage(self):
		return float(self.hits)/float(self.atbats)

#script starts here
import sys,os,re
if len(sys.argv) < 2:
	sys.exit("Usage: %s filename" % sys.argv[0])
filename = sys.argv[1]
if not os.path.exists(filename):
	sys.exit("Error: File '%s' not found" % sys.argv[1])
reg_title = re.compile(r"^===.+?===\n$") #regular expression for game title
reg_player = re.compile(r"^(.+?) batted (\d+?) times with (\d+?) hits and (\d+?) runs\n$") #regular expression for player statistics
allplayers = dict() # creates dictionary that stores all players
f=open(filename)
for line in f:
	if line=="\n": #newline
		continue
	elif reg_title.match(line) is not None:  #the title for a game
		continue
	else:
		match=reg_player.match(line)
		if match is not None:
			name = match.group(1)
			atbats = int(match.group(2))
			hits = int(match.group(3))
			runs = int(match.group(4))
			if (allplayers.has_key(name)):
                            p = allplayers.get(name)
			else:
                            p = Player(name)
                            allplayers[name]=p
			p.addAtbats(atbats)
			p.addHits(hits)
			p.addRuns(runs)
f.close()
#sorts Player objects in descending order based on batting average
sortedplayers = sorted(allplayers.values(), key=lambda v: v.getBattingAverage(), reverse=True)
for p in sortedplayers:
	pname = p.getName()    
    	batting_average = p.getBattingAverage()
	#prints out name of player and batting average in 4 digits, 3 decimal places
    	print "%s: %4.3f" % (pname,batting_average)