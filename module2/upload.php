<?
	session_start();
	if (!isset($_SESSION['user'])) {
		header("Location: login.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Upload Page</title>
		<link rel="stylesheet" type="text/css" href="custom.css">
	</head>
	<body>
		Logged in as: <? echo $_SESSION['user']; ?><br>
		<form enctype="multipart/form-data" action="uploadfile.php" method="POST">
			<p>
			<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
			<input type="hidden" name="token" value="<? echo $_SESSION['token']; ?>">
			<label for="uploadfile_input">Choose a file to upload:</label> 
			<input type="file"  multiple="true" id="uploadfile_input" name="uploadedfile[]"/>
			</p>
			<p>
			<input type="submit" value="Upload File" />
			</p>
		</form>
		<a class="myButton" href="profile.php">Profile</a>
		<a class="logout" href="logout.php">Log out</a>
	</body>
</html>