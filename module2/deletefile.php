<?php
session_start();
 
$filename = $_GET['file'];
 $error="";
// We need to make sure that the filename is in a valid format; if it's not, display an error and leave the script.
// To perform the check, we will use a regular expression.
if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
	$error="Invalid filename";
}
 
// Get the username and make sure that it is alphanumeric with limited other characters.
// You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
// since we will be concatenating the string to load files from the filesystem.
$username = $_SESSION['user'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	$error="Invalid username";
}
if ($error=="") {
	$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
 	if (unlink($full_path)) {
		header("Location: download.php");
	} else {
		header("Location: download.php?error=did-not-delete");
	}
} else {
	header("Location: download.php?error=".$error);
}
 
?>