<?
	session_start();
	if (!isset($_SESSION['user'])) {
		header("Location: login.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Download Page</title>
		<link rel="stylesheet" type="text/css" href="custom.css">
		<link rel="stylesheet" type="text/css" href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css">

	</head>
	<body>
		Logged in as: <? echo $_SESSION['user']; ?><br>
		<a class="myButton" href="profile.php">Profile</a><br>
		<a class="logout" href="logout.php">Log out</a><br>
		<ul>
		<?
			$dir = "/srv/uploads/".$_SESSION['user'];
			$dh = opendir($dir);
			while (false !== ($filename = readdir($dh))) {
    				if ($filename!="."&&$filename!="..")
					printf("<li><a href=\"downloadfile.php?file=%s\">%s</a>&nbsp;&nbsp;&nbsp;<a class=\"close\" href=\"deletefile.php?file=%s\"><i class=\"icon-trash\"></i></a></li>",$filename,$filename,$filename);
					echo PHP_EOL;
			}
		?>
		</ul>
	</body>
</html>