<?php
session_start();
$error=""; //for debugging
// Get the username and make sure that it is alphanumeric with limited other characters.
// You shouldn't allow usernames with unusual characters anyway, but it's always best to perform a sanity check
// since we will be concatenating the string to load files from the filesystem.
$username = $_SESSION['user'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	$error="Invalid username";
}
if ($error=="") {
	$picname="default.gif";
	$dir = "/srv/uploads/profile_pic";
	$dh = opendir($dir);
	while (false !== ($filename = readdir($dh))) {
		$f = explode('.',$filename);
    		if ($filename!="."&&$filename!=".."&&$f[0]==$username) {
			$picname=$filename;
		}
	}
	$full_path = sprintf("/srv/uploads/profile_pic/%s",$picname);
 	
	// Now we need to get the MIME type (e.g., image/jpeg).  PHP provides a neat little interface to do this called finfo.
	$finfo = new finfo(FILEINFO_MIME_TYPE);
	$mime = $finfo->file($full_path);
	 
	// Finally, set the Content-Type header to the MIME type of the file, and display the file.
	header("Content-Type: ".$mime);
	readfile($full_path);
} else {
	//header("Location: profile.php");
}
 
?>