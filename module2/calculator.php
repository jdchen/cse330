<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Simple Calculator</title>
		<style type="text/css">
			div#calculator_input{
				margin:0px 0px 0px 100px;
				padding:10px;
			}
			div#calculation_result{
				background-color: #C0C0C0;
				border: 5px solid black;
				margin:20px 100px 0px 100px;
				padding:50px;
				text-align:center;
			}
			pre{
				font-size:30px;
			}
		</style>
	</head>
	<body>
		<div id="calculator_input">
			<h1>Calculator</h1>
			<form action="calculator.php" method="GET">
				<?
					if (isset($_GET['operand1'])&&isset($_GET['operand2'])
						&&is_numeric($_GET['operand1'])&&is_numeric($_GET['operand2'])) {
						echo "First number:<input name=\"operand1\" type=\"text\" maxlength=\"14\" value=\"".(float)$_GET['operand1']."\"/><br>".PHP_EOL;
						echo "\t\t\t\tSecond number:<input name=\"operand2\" type=\"text\" maxlength=\"14\" value=\"".(float)$_GET['operand2']."\"/><br><br>".PHP_EOL;
					} else {
						echo "First number:<input name=\"operand1\" type=\"text\" maxlength=\"14\"/><br>".PHP_EOL;
						echo "\t\t\t\tSecond number:<input name=\"operand2\" type=\"text\" maxlength=\"14\"/><br><br>".PHP_EOL;
					}
				?>
				<input type="radio" name="calculation_type" value="Add" <? if(isset($_GET['calculation_type'])&&$_GET['calculation_type']=='Add') echo "checked"; ?> > Add<br>
				<input type="radio" name="calculation_type" value="Subtract" <? if(isset($_GET['calculation_type'])&&$_GET['calculation_type']=='Subtract') echo "checked"; ?> > Subtract<br>
				<input type="radio" name="calculation_type" value="Multiply" <? if(isset($_GET['calculation_type'])&&$_GET['calculation_type']=='Multiply') echo "checked"; ?> > Multiply <br>
				<input type="radio" name="calculation_type" value="Divide" <? if(isset($_GET['calculation_type'])&&$_GET['calculation_type']=='Divide') echo "checked"; ?> > Divide<br><br>
				<input type="submit" value="Calculate"/>
			</form>
		</div>
		<div id="calculation_result">
			<?
				if (isset($_GET['operand1'])&&isset($_GET['operand2'])&&isset($_GET['calculation_type'])) {
					if (is_numeric($_GET['operand1'])&&is_numeric($_GET['operand2'])) {
						$op1=(float)$_GET['operand1'];
						$op2=(float)$_GET['operand2'];
						switch ($_GET['calculation_type']) {
							case "Add":
								$res=$op1+$op2;
								$sign="+";
								break;
							case "Subtract":
								$res=$op1-$op2;
								$sign= "-";
								break;
							case "Multiply":
								$res=$op1*$op2;
								$sign= "*";
								break;
							case "Divide":
								if ($op2==0) break;
								$res=$op1/$op2;
								$sign= "/";
								break;
						}
						if (!isset($res)) {
							echo "<pre>Invalid operation</pre>".PHP_EOL;
						} else {
							echo "<pre>".htmlentities($op1)."  ".htmlentities($sign)."  ".htmlentities($op2)."  =  ".htmlentities($res)."</pre>".PHP_EOL;
						}
					} else {
						echo "<pre>Invalid operation</pre>".PHP_EOL;
					}
				} else {
					echo "<pre>No result available</pre>".PHP_EOL;
				}
			?>
		</div>
	</body>
</html>