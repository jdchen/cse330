<?
	session_start();
	if (!isset($_SESSION['user'])) {
		header("Location: login.php");
		exit;
	}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Profile Page</title>
		<style type="text/css"> 
		iframe {
			height:500px;
			width:1000px;
			/* seamless, scrolling do not work in css */
		}	
		</style>
		<link rel="stylesheet" type="text/css" href="custom.css">

	</head>
	<body>
		<h1>Profile Page</h1><br>
		Logged in as: <? echo $_SESSION['user']; ?><br>
		<a class="logout" href="logout.php">Log out</a>
		<a class="myButton" href="upload.php">Upload</a>
		<a class="myButton" href="download.php">Download</a>
		<br>
		<iframe seamless="true" scrolling="no" src="profilepic.php"></iframe><br>
		<form enctype="multipart/form-data" action="uploadprofilepic.php" method="POST">
			<p>
			<input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
			<input type="hidden" name="token" value="<? echo $_SESSION['token']; ?>">
			<label for="uploadfile_input">Upload your profile pic:</label> 
			<input name="uploadedfile" type="file" id="uploadfile_input" accept="image/*"/>
			</p>
			<p>
			<input type="submit" value="Upload File" />
			</p>
		</form>	
	</body>
</html>