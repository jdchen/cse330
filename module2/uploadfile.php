<?php
session_start();
 
// Get the filename and make sure it is valid
$error=""; //for debugging
// Get the username and make sure it is valid
$username = $_SESSION['user'];
if( !preg_match('/^[\w_\-]+$/', $username) ){
	$error="Invalid username:".$username;
	header("Location: upload.php");
	exit;
}
$size = count($_FILES['uploadedfile']['name']);
if ($size>20) {
	$error="File limit exceeded";
	header("Location: upload.php");
	exit;
}
for ($i=0; $i<$size;$i++) {
	$filename = basename($_FILES['uploadedfile']['name'][$i]);
	$filename = str_replace(' ','_',$filename); //replace space with underscore
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		$error="Invalid filename".$filename;
		header("Location: upload.php");
		exit;
	}else {
		$full_path = sprintf("/srv/uploads/%s/%s", $username, $filename);
		if( !move_uploaded_file($_FILES['uploadedfile']['tmp_name'][$i], $full_path) ){
			$error="Cannot move uploaded file";
			header("Location: upload.php");
			exit;
		}
	}
} 
header("Location: download.php");
 
?>