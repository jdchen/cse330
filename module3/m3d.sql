use wustl;
select * from grades;
select * from courses where school_code='L';
select students.first_name,students.last_name,students.id,grades.grade
	from students, grades
	where students.id = grades.student_id
		and grades.school_code='E'
		and grades.dept_id=81
		and grades.course_code='330S';
select students.first_name, students.last_name, students.email_address, avg(grades.grade)
	from students, grades
	where students.id = grades.student_id
	group by students.id
	having avg(grades.grade) < 50;
select students.id, students.email_address, avg(grades.grade)
	from students, grades
	where students.id = grades.student_id
		and students.first_name='Jack'
		and students.last_name='Johnson'
	group by students.id, students.email_address;
