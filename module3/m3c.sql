use wustl;
insert ignore into courses(school_code,dept_id,course_code,name)
	select
		school_code,
		dept_id,
		'330S',
		'Rapid Prototype Development and Creative Programming'
	from departments where abbreviation='CSE';
insert ignore into students values(88,'Ben','Harper','bharper@ffym.com');
insert ignore into students values(202,'Matt','Freeman','mfreeman@kickinbassist.net');
insert ignore into students values(115,'Marc','Roberge','mroberge@ofarevolution.us');
-- grade id is auto increment 
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(88,35.5,'E',81,'330S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(88,0,'E',81,'436S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(88,95,'S',80,'4000');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(202,100,'E',81,'330S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(202,90.5,'E',81,'436S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(202,94.8,'S',80,'4000');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(115,75,'E',81,'330S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(115,37,'E',81,'436S');
insert into grades(student_id,grade,school_code,dept_id,course_code)
	values(115,45.5,'S',80,'4000');
-- select * from students;