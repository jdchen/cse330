create database if not exists news;
-- create user 'news'@'localhost' identified by 'news_pass';
grant all on news.* to 'news'@'localhost';
use news;
create table if not exists user_login
(
	id int unsigned not null auto_increment,
	username varchar(100) not null,
	password char(32) not null,
	primary key (id)
);
create table if not exists story
(
	story_id int unsigned not null auto_increment,
	user_id int unsigned not null,
	url varchar(200),
	content text,
	date_modified timestamp default current_timestamp on update current_timestamp,
	primary key (story_id),
	foreign key (user_id) references user_login (id)
);
create table if not exists comment
(
	comment_id int unsigned not null auto_increment,
	story_id int unsigned not null,
	user_id int unsigned not null,
	content text,
	date_modified timestamp default current_timestamp on update current_timestamp,
	primary key (comment_id),
	foreign key (user_id) references user_login (id),
	foreign key (story_id) references story (story_id)
);

	




