<?
	session_start();
	if (!isset($_SESSION['user'])) {
		header("Location: login.php");
		exit;
	}
	$token = $_POST['token'];
	if (!isset($_SESSION['token'])||$token!=$_SESSION['token']) {
		header("Location: home.php");
		exit;
	}
	$submit=$_POST['submit'];
	$story_id=$_POST['story_id'];
	if ($submit=='delete') {
		require "database.php";
		$stmt = $mysqli->prepare("DELETE FROM comment WHERE comment_id=?");
		$stmt->bind_param('d', $id);
		$id=$_GET['comment_id'];
		$stmt->execute();
		$stmt->close();
		header("Location: comment.php?story_id=$story_id");
		exit;
	}
	else if ($submit=='save') {
		require "database.php";
		$stmt = $mysqli->prepare("UPDATE comment SET content=? WHERE comment_id=?");
		$stmt->bind_param('sd',$content,$id);
		$content=$_POST['edit'];
		$id=$_GET['comment_id'];
		$stmt->execute();
		$stmt->close();
		header("Location: comment.php?story_id=$story_id");
	}
	else if ($submit=='edit'){
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>News Page</title>
	</head>
	<body>
		<a href="home.php">Return to home page</a>
<?
		require "database.php";
		$stmt = $mysqli->prepare("SELECT comment.content from comment where comment_id=?");
		$stmt->bind_param('d', $id);
		$id=$_GET['comment_id'];
		$stmt->execute();
		$stmt->bind_result($c);
		$stmt->fetch();

		echo "<form action=\"edit.php?comment_id=".$_GET['comment_id']."\" method=\"POST\">";
		echo "<input type=\"hidden\" name=\"token\" value=\"".$_SESSION['token']."\"/>";
		echo "<input type=\"hidden\" name=\"story_id\" value=\"".$_POST['story_id']."\"/>";
		echo "<textarea name=\"edit\" maxlength=\"200\" rows=\"10\" cols=\"50\">".htmlentities($c)."</textarea><br>";
		echo "<input type = \"submit\" name=\"submit\" value = \"save\" />";
		echo "</form>";
		$stmt->close();
		
	}
	else echo "No comment specified";
?>
</body>
</html>