<?
	session_start();
	if (!isset($_SESSION['user'])) {
		$guest=true;
	} else $guest=false;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>News Page</title>
	</head>
	<body>
		<h1>News Page</h1>
		<?
		if ($guest) {
			echo "Logged in as: Guest <br>";
			echo "<a href=\"login.php\">Log in as registered user</a>";
		} else {
		?>
		Logged in as: <? echo $_SESSION['user']; ?><br>
		<a class="logout" href="logout.php">Log out</a>	
		<form action="home.php" method="GET">
			<input type="hidden" name="sort" value="date_asc"/>
			<input type="submit" value="Sort by date ascending"/>
		</form>
		<form action="home.php" method="GET">
			<input type="hidden" name="sort" value="date_desc"/>
			<input type="submit" value="Sort by date descending"/>
		</form>
		<form action="home.php" method="GET">
			<input type="hidden" name="sort" value="title_asc"/>
			<input type="submit" value="Sort by title ascending"/>
		</form>
		<form action="home.php" method="GET">
			<input type="hidden" name="sort" value="title_desc"/>
			<input type="submit" value="Sort by title descending"/>
		</form>
		<br><br>
		<?
		}
		require "database.php";
		if (!isset($_GET['sort'])||$_GET['sort']=='date_desc')
			$stmt = $mysqli->prepare("SELECT story_id,username,title,url,description,content,date_modified FROM story,user_login WHERE story.user_id=user_login.id ORDER BY date_modified DESC");
		else if ($_GET['sort']=='date_asc')
			$stmt = $mysqli->prepare("SELECT story_id,username,title,url,description,content,date_modified FROM story,user_login WHERE story.user_id=user_login.id ORDER BY date_modified ASC");
		else if ($_GET['sort']=='title_asc')
			$stmt = $mysqli->prepare("SELECT story_id,username,title,url,description,content,date_modified FROM story,user_login WHERE story.user_id=user_login.id ORDER BY title ASC");
		else if ($_GET['sort']=='title_desc')
			$stmt = $mysqli->prepare("SELECT story_id,username,title,url,description,content,date_modified FROM story,user_login WHERE story.user_id=user_login.id ORDER BY title DESC");
		$stmt->execute();
		$stmt->bind_result($story_id,$u,$t,$l,$d,$s,$date);
		while ($stmt->fetch()!=null){
		echo "<a href=\"".htmlentities($l)."\"><h3>".htmlentities($t)."</h3></a>";
		echo "<i>".htmlentities($d)."</i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		echo "Posted by: ".htmlentities($u)."&nbsp;&nbsp;".htmlentities($date)."<br>";
		echo htmlentities($s)."<br>";
		//youtube embed
		if (strpos($l,'http://www.youtube.com/watch?v=')!==false) {
			$ll=substr($l,31);
			echo "<iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/".$ll."\"  allowfullscreen></iframe>";
		}
		echo "<br><a href=\"comment.php?story_id=$story_id\">comments</a>";
		if (!$guest&&$_SESSION['user']==$u){
			echo "<form action=\"editstory.php?story_id=$story_id\" method=\"POST\">";
				echo "<input type=\"hidden\" name=\"token\" value=\"".$_SESSION['token']."\"/>";
				echo "<input type=\"hidden\" name=\"story_id\" value=".$story_id."/>";
				echo "<input type=\"submit\" name=\"submit\" value=\"edit\"/>&nbsp;&nbsp;&nbsp;";
				echo "<input type=\"submit\" name=\"submit\" value = \"delete\"><br>";
			echo "</form>";
		}
		}
		$stmt->close();
		if (!$guest) {
		?>
		<br>
		<form action="story.php" method="POST">
			<input type="hidden" name="token" value="<? echo $_SESSION['token']; ?>">
			<input type="hidden" name="story_id" value="<? if (isset($_GET['story_id'])) echo $_GET['story_id']; ?>">
			Title of story <input type="text" name="title"/><br>
			Link to story <input type="text" name="link"/><br>
			Description<br>
			<textarea name="description" maxlength="200" rows="4" cols="50"></textarea></br>
			Story<br>
			<textarea name="story" maxlength="200" rows="10" cols="50"></textarea></br>
			<input type="submit" value="Submit" />
		</form>
		<?}?>
	</body>
</html>