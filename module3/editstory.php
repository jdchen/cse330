<?
	session_start();
	if (!isset($_SESSION['user'])) {
		header("Location: login.php");
		exit;
	}
	$token = $_POST['token'];
	if (!isset($_SESSION['token'])||$token!=$_SESSION['token']) {
		header("Location: home.php");
		exit;
	}
	$submit=$_POST['submit'];
	if ($submit=='delete') {
		require "database.php";
		$stmt = $mysqli->prepare("DELETE FROM comment WHERE story_id=?");
		$stmt->bind_param('d', $id);
		$id=$_GET['story_id'];
		$stmt->execute();
		$stmt->close();
		$stmt = $mysqli->prepare("DELETE FROM story WHERE story_id=?");
		$stmt->bind_param('d', $id2);
		$id2=$_GET['story_id'];
		$stmt->execute();
		$stmt->close();
		header("Location: home.php");
		exit;
	}
	else if ($submit=='save') {
		require "database.php";
		$stmt = $mysqli->prepare("UPDATE story SET title=?,url=?,description=?,content=? WHERE story_id=?");
		$stmt->bind_param('ssssd',$t,$l,$d,$s,$id);
		$id=$_POST['story_id'];
		$t=$_POST['title'];
		$l=$_POST['link'];
		$d=$_POST['editdescrip'];
		$s=$_POST['editstory'];
		$stmt->execute();
		$stmt->close();
		header("Location: home.php");
	}
	else if ($submit=='edit') {
?>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>News Page</title>
	</head>
<body>
	<a href="home.php">Return to home page</a>

<?
		require "database.php";
		$stmt = $mysqli->prepare("SELECT story.title FROM story WHERE story_id=?");
		$stmt = $mysqli->prepare("SELECT title,url,description,content FROM story WHERE story_id=?");
		$stmt->bind_param('d', $id);
		$id=$_GET['story_id'];
		$stmt->execute();
		$stmt->bind_result($t,$l,$d,$s);
		$stmt->fetch();

		echo "<form action=\"editstory.php?story_id=".$_GET['story_id']."\" method=\"POST\">";
		echo "<input type=\"hidden\" name=\"token\" value=\"".$_SESSION['token']."\"/>";
		echo "<input type=\"hidden\" name=\"story_id\" value=\"".$_POST['story_id']."\"/>";
		echo "Title of Story <input type=\"text\" name=\"title\" value=\"".htmlentities($t)."\"/><br>";
		echo "Link to Story <input type=\"text\" name=\"link\" value=\"".htmlentities($l)."\"/><br>";
		echo "Description<br>";
		echo "<textarea name=\"editdescrip\" maxlength=\"200\" rows=\"4\" cols=\"50\">".htmlentities($d)."</textarea><br>";
		echo "Story<br>";
		echo "<textarea name=\"editstory\" maxlength=\"200\" rows=\"10\" cols=\"50\">".htmlentities($s)."</textarea><br>";
		echo "<input type = \"submit\" name=\"submit\" value = \"save\" />";
		echo "</form>";
		$stmt->close();
		
	} else echo "No story specified";

?>
</body>
</html>