<?
	require 'database.php';

	$u = $_POST['user'];
	$p = $_POST['pass'];
	$p2 = $_POST['pass2'];
	$t = $_POST['type'];
	if ($t=='register') {
		if (!isset($u)||!ctype_alnum($u)||!isset($p)||!ctype_alnum($p)
			||$p!=$p2||strlen($p)<6) {
			header("Location: register.php?err=1");
		} else {	
			$stmt = $mysqli->prepare("SELECT COUNT(*) FROM user_login WHERE username=?");
			$stmt->bind_param('s', $user);
			$user = $u;
			$stmt->execute();
			$stmt->bind_result($cnt);
			$stmt->fetch();
			$stmt->close();
			if ($cnt>0) {header("Location: register.php?err=1"); exit;}
			$stmt = $mysqli->prepare("INSERT INTO user_login(username,password) VALUES(?,?);");
			$stmt->bind_param('ss', $user,$pass);
			$user = $u;
			$pass = hash('sha256',$p);
			$stmt->execute();
			$stmt->close();
			$stmt = $mysqli->prepare("SELECT id FROM user_login WHERE username=?");
			$stmt->bind_param('s', $user);
			$user = $u;
			$stmt->execute();
			$stmt->bind_result($user_id);
			$stmt->fetch();
			$stmt->close();
			$mysqli->close();
			session_start();
			$_SESSION['user_id'] = $user_id;
			$_SESSION['user'] = $u;
			$_SESSION['token'] = substr(md5(rand()), 0, 10);
			header("Location: home.php");
		}
	} else if ($t=='login') {
		if (!isset($u)||!ctype_alnum($u)||!isset($p)||!ctype_alnum($p)) {
			header("Location: login.php?err=1");
		} else {	
			$stmt = $mysqli->prepare("SELECT COUNT(*), id, password FROM user_login WHERE username=?");
			$stmt->bind_param('s', $user);
			$user = $u;
			$stmt->execute();
			$stmt->bind_result($cnt, $user_id, $pwd_hash);
			$stmt->fetch();
			$stmt->close();
			$mysqli->close();
			$pwd_guess = $p;
			if( $cnt == 1 && hash('sha256',$pwd_guess)==$pwd_hash){
				session_start();
				$_SESSION['user_id'] = $user_id;
				$_SESSION['user'] = $u;
				$_SESSION['token'] = substr(md5(rand()), 0, 10);
				header("Location: home.php");
			}else{
				header("Location: login.php?err=1");
			}
		}
	} else header("Location: login.php");

?>
