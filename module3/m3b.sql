use wustl;
create table if not exists students
(
	id mediumint unsigned,
	first_name varchar(50),
	last_name varchar(50),
	email_address varchar(50),
	primary key (id)
);
create table if not exists departments
(
	school_code enum('L','B','A','F','E','T','I','W','S','U','M'),
	dept_id tinyint unsigned,
	abbreviation varchar(9) not null,
	dept_name varchar(200) not null,
	primary key (school_code,dept_id)
);
create table if not exists courses
(
	school_code enum('L','B','A','F','E','T','I','W','S','U','M'),
        dept_id tinyint unsigned,
	course_code char(5),
	name varchar(150),
	primary key (course_code,school_code,dept_id),
	foreign key (school_code,dept_id)
		 references departments (school_code,dept_id)
);
create table if not exists grades
(
	pk_grade_ID int unsigned auto_increment,
	student_id mediumint unsigned,
	grade decimal(5,2) not null,
	school_code enum('L','B','A','F','E','T','I','W','S','U','M'),
        dept_id tinyint unsigned,
	course_code char(5),
	primary key (pk_grade_ID),
	foreign key (student_id) references students (id),
	foreign key (course_code,school_code,dept_id)
		 references courses (course_code,school_code,dept_id)
);
-- duplicate data is ignored
load data local infile 'students_data.txt' into table students;
load data local infile 'departments_data.txt' into table departments;
load data local infile 'courses.txt' into table courses;
load data local infile 'grades_data.txt' into table grades;

	


