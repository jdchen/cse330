<?
	session_start();
	if (!isset($_SESSION['user'])) {
		$guest=true;
	} else $guest=false;
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Comments</title>
	</head>
	<body>
		<?
		if ($guest) {
			echo "Logged in as: Guest <br>";
			echo "<a href=\"login.php\">Log in as registered user</a>&nbsp;<a href=\"home.php\">Home</a>";
		} else {
		?>
		Logged in as: <? echo $_SESSION['user']; ?><br>
		<a class="logout" href="logout.php">Log out</a>&nbsp;<a href="home.php">Home</a><br><br>
		<?
		}
		require "database.php";
		$stmt = $mysqli->prepare("SELECT story_id,user_id,title,url,description,content FROM story WHERE story_id=?");
		$stmt->bind_param('d', $id);
		if (!isset($_GET['story_id'])) {echo "<br>No story specified";}
		else {
		$id=$_GET['story_id'];
		$stmt->execute();
		$stmt->bind_result($story_id,$u,$t,$l,$d,$s);
		$stmt->fetch();
		echo "<h3>".htmlentities($t)."</h3>";
		echo "<i>".htmlentities($d)."</i><br>";
		echo htmlentities($s)."<br><br>";
		?>

		<?
		require "database.php";
		$stmt = $mysqli->prepare("SELECT comment.comment_id,user_login.username,comment.content,comment.date_modified from user_login,comment where user_login.id=comment.user_id and story_id=?");
		$stmt->bind_param('d', $id);
		$id=$_GET['story_id'];
		$stmt->execute();
		$stmt->bind_result($comment_id,$u,$c,$d);
		while ($stmt->fetch()!=null){
		echo "<strong>".htmlentities($u)."</strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".htmlentities($d)."<br>";
		echo htmlentities($c)."<br>";
		if (!$guest&&$_SESSION['user']==$u){
			echo "<form action=\"edit.php?comment_id=$comment_id\" method=\"POST\">";
				echo "<input type=\"hidden\" name=\"token\" value=\"".$_SESSION['token']."\"/>";
				echo "<input type=\"hidden\" name=\"story_id\" value=".$_GET['story_id']."/>";
				echo "<input type=\"submit\" name=\"submit\" value=\"edit\"/>&nbsp;&nbsp;&nbsp;";
				echo "<input type=\"submit\" name=\"submit\" value = \"delete\"><br>";
			echo "</form>";
		}
		echo "<br>";
		}
		$stmt->close();
		if (!$guest) {
		?>

		
	<form action="commenthelp.php" method="POST">
		<input type="hidden" name="token" value="<? echo $_SESSION['token']; ?>">
		<br>Your Comments:<br>
		<textarea name="comment" maxlength="200" rows="10" cols="50"></textarea><br>
		<input type="submit" value="Submit" />
		<?
		echo "<input type=\"hidden\" name=\"story_id\" value=".$_GET['story_id']."/>";
		
		?>
	</form>
	<?}}?>
</body>
</html>


