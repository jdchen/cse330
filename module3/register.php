<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8"/>
		<title>Register Page</title>
	</head>
	<body>
		<h1>Register Page</h1>
		<ul>
			<li>User name and password cannot be more than 30 characters long</li>
			<li>User name must be alphanumeric</li>
			<li>Passwords must be alphanumeric</li>
			<li>Passwords must be more than 6 characters long</li>
		</ul>
		<form action="verify.php" method="POST">
			User name: <input type="text" name="user" maxlength="30"/><br>
			Password: <input type="password" name="pass" maxlength="30"/><br>
			Confirm Password: <input type="password" name="pass2" maxlength="30"/><br>
			<input type="hidden" name="type" value="register"/>
			<? if (isset($_GET['err']))echo "***Invalid username or password***";?><br><br>
			<input type="submit" value="Register"/>
		</form>
		<br><a href="home.php">Go to home page </a> (You may view as guest)
		<br><a href="login.php">Go to login page </a>
	</body>
</html>