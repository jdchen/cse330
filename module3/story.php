<?
	require 'database.php';

	if (!isset($_POST['title'])||trim($_POST['title'])=='') $title="*Default title*";
	else $title=$_POST['title'];
	if (!isset($_POST['link'])||trim($_POST['link'])=='') $link="*home.php*";
	$link=$_POST['link'];
	if (!isset($_POST['description'])||trim($_POST['description'])=='') $description="*Default description*";
	else $description=$_POST['description'];
	$story=$_POST['story'];
	$token=$_POST['token'];
	session_start();
	$user = $_SESSION['user_id'];
	if (!isset($_SESSION['token'])||$token!=$_SESSION['token']) {
		header("Location: home.php");
		exit;
	}


	$stmt = $mysqli->prepare("INSERT INTO story(user_id,title,url,description,content) VALUES(?,?,?,?,?);");
	$stmt->bind_param('dssss', $u,$t,$l,$d,$s);
	$u = $user;
	$t = $title;
	$l=$link;
	$d=$description;
	$s=$story;
	$stmt->execute();
	$stmt->close();

	header("Location: home.php");

?>