<?
	require 'database.php';

	$story_id=$_POST['story_id'];
	$content=$_POST['comment'];
	$token=$_POST['token'];
	
	session_start();
	$user = $_SESSION['user_id'];
	if (!isset($_SESSION['token'])||$token!=$_SESSION['token']) {
		header("Location: home.php");
		exit;
	}

	$stmt = $mysqli->prepare("INSERT INTO comment(user_id,story_id,content) VALUES(?,?,?);");
	$stmt->bind_param('dds', $u,$s,$c);
	$u = $user;
	$s = $story_id;
	$c=$content;
	$stmt->execute();
	$stmt->close();

	header("Location: comment.php?story_id=$story_id");

?>